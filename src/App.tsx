import React, { useState, useEffect } from 'react';
import './styles/App.css';
import { API_URL } from './utils/constants';
import NavBar from './components/navBar';
import EventCard from './components/eventCard';
import { CardProps } from './utils/types';


export default function App() {
  const [data, setData] = useState<CardProps[]>([])
  const [searchInput, setSearchInput] = useState("")
  const [cartEventsCount, setCartEventsCount] = useState(0)
  const [cartEvents, setCartEvents] = useState<CardProps[]>([])
  data.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
  const newDates = Array.from(new Set(data.map(i => new Date(i.startTime).toLocaleDateString())))
  const index = newDates.indexOf("Invalid Date")
  newDates.splice(index, 1)

  useEffect(() => {
    fetch(API_URL).then((res: any) => res.json()).then((res: any) => {
      const newRes = res.map((event: any) => {
        let newName: string = event.venue.name
        if (/ /.test(event.venue.name)) {
          newName = event.venue.name.replace(/ /g, "+")
        }
        return {
          ...event,
          venue: {
            ...event.venue,
            direction: event.venue.direction.replace("Colour+Factory", newName)
          },
        }
      })
      setData(newRes)
    })
  }, [])

  return (
    <div className="app">
      <div style={{ position: "sticky", top: 0, zIndex: 20 }}>
        <NavBar searchCallback={(e: string) => setSearchInput(e)} cartEventsCount={cartEventsCount} cartEvents={cartEvents} />
      </div>
      {newDates.map((date: string) => {
        return (
          <div key={date}>
            <div style={{ position: "sticky", top: 64, padding: 20, background: "rgba(0, 0, 0, 0.8", zIndex: 10 }}>
              <h1 style={{ color: "#EB4298" }}>{date}</h1>
            </div>
            <div className='card-view'>
              {data.map((i: CardProps, index) => {
                return new Date(i.startTime).toLocaleDateString() === date && i.title.toLowerCase().includes(searchInput.toLowerCase()) ? (
                  <div className='card-component' key={i._id}>
                    <EventCard
                      title={i.title}
                      flyerFront={i.flyerFront}
                      date={i.date}
                      startTime={i.startTime}
                      endTime={i.endTime}
                      venue={i.venue}
                      pick={i.pick}
                      artists={i.artists}
                      city={i.city}
                      country={i.country}
                      private={i.private}
                      addCallback={() => {
                        setCartEventsCount(cartEventsCount + 1)
                        setCartEvents([...cartEvents, data[index]])
                        data.splice(index, 1)
                      }
                      }
                    />
                  </div>
                ) : null
              })
              }
            </div>
          </div>
        )
      })}

    </div>
  );
}

