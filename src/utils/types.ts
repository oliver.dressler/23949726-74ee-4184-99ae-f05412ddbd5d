interface VenueType {
    id: string,
    name: string,
    contentUrl: string,
    live: Boolean,
    direction: string
}

interface PickType {
    id: string,
    blurb: string
}

interface ArtistsType {
    id: string,
    name: string
}

export interface CardProps {
    _id?: string,
    title: string,
    flyerFront: string,
    date: string,
    startTime: string,
    endTime: string,
    venue: VenueType,
    pick?: PickType,
    artists?: ArtistsType[]
    city?: string,
    country?: string,
    private?: Boolean
    addCallback?: any
}