import * as React from 'react';
import { styled, alpha, createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import "../styles/cart.css"
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Badge from '@mui/material/Badge';
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { Divider } from '@mui/material';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';


const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
}));

const AlignItemsList = (props: any) => {
    return props.data.length > 0 ? (
        <div>
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: grey[900] }}>
                {props.data?.map((cart: any) => {
                    return (
                        <ListItem alignItems="flex-start" key={cart._id} className="cart-item">
                            <ListItemAvatar>
                                <Avatar sx={{ bgcolor: "#EB4298" }}>{cart.title.slice(0, 1).toUpperCase()}</Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary={cart.title}
                            />
                            <Divider variant="inset" component="li" />
                        </ListItem>
                    )
                })}
            </List>
        </div>
    ) : <List sx={{ width: '100%', maxWidth: 360, minHeight: 100, height: "100%", minWidth: 360, bgcolor: grey[900], display: "flex", justifyContent: "center", alignItems: "center" }}>
        <h3 style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
            Warenkorb leer!
        </h3>
    </List>;
}


export default function NavBar(props: any) {
    const [visible, setVisible] = React.useState(false)
    const theme = createTheme({
        palette: {
            primary: {
                main: grey[900],
            },
            secondary: {
                main: "#EB4298"
            }
        },
    });
    return (
        <ThemeProvider theme={theme}>
            <Box sx={{ flexGrow: 1 }} color="primary">
                <AppBar position="static">
                    <Toolbar>
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon />
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Search…"
                                inputProps={{ 'aria-label': 'search' }}
                                onChange={(e) => props.searchCallback(e.target.value)}
                            />
                        </Search>
                        <Box sx={{ flexGrow: 1 }} />
                        <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                            <IconButton size="large" color="inherit" onClick={() => setVisible(!visible)}>
                                <Badge badgeContent={props.cartEventsCount} color="secondary">
                                    <ShoppingCartOutlinedIcon />
                                </Badge>
                            </IconButton>
                            {visible ?
                                <div style={{ position: "absolute", top: 72, right: 10 }}>
                                    <AlignItemsList
                                        data={props.cartEvents}
                                    />
                                </div>
                                : null}
                        </Box>
                    </Toolbar>
                </AppBar>
            </Box>
        </ThemeProvider>
    );
}




