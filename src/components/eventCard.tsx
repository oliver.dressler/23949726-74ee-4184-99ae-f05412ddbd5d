import * as React from 'react';
import '../styles/eventCard.css'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { grey } from '@mui/material/colors';
import AddLocationIcon from '@mui/icons-material/AddLocation';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { CardProps } from '../utils/types';

export default function EventCard(props: CardProps) {

    return (
        <Card sx={{ minWidth: 500, maxWidth: 500, height: "auto" }}>
            <CardHeader
                avatar={
                    <Avatar sx={{ bgcolor: "#EB4298" }} aria-label="recipe">
                        {props.title.slice(0, 1).toUpperCase()}
                    </Avatar>
                }
                sx={{ bgcolor: grey[900], color: "#fff" }}
                title={props.title}
                style={{ fontWeight: "700" }}
            />
            {!!props.flyerFront ?
                <CardMedia
                    component="img"
                    image={props.flyerFront}
                    height="auto"
                    width="auto"
                    alt={props.title}
                />
                :
                <div className='card-empty-flyer'>
                    <h3>{props.title}</h3>
                </div>
            }
            <CardContent sx={{ bgcolor: grey[900], color: "#fff" }}>
                <div className='card-bottom-location'>
                    <a href={props.venue.direction} target="_blank" rel='noreferrer' style={{ padding: 0 }}>
                        <IconButton aria-label="add to favorites" style={{ padding: 0 }}>
                            <AddLocationIcon sx={{ color: "#EB4298" }} fontSize='large' />
                        </IconButton>
                    </a>
                    <Typography variant="body1" sx={{ color: "#fff", fontWeight: "700" }}>
                        {props.venue.name}
                    </Typography>
                </div>

                <Typography variant="body1" sx={{ color: grey[600] }}>
                    | Starts: {new Date(props.startTime).toLocaleDateString()}, {new Date(props.startTime).toLocaleTimeString()}
                </Typography>
                <Typography variant="body1" sx={{ color: grey[600] }}>
                    | Ends: {new Date(props.endTime).toLocaleDateString()}, {new Date(props.endTime).toLocaleTimeString()}
                </Typography>
            </CardContent>
            <CardContent sx={{ bgcolor: grey[900], color: "#fff" }} style={{ display: "flex", justifyContent: 'right' }}>
                <IconButton aria-label="add to favorites" style={{ padding: 0 }}>
                    <AddCircleIcon sx={{ color: "#EB4298" }} fontSize='large' onClick={() => props.addCallback([props._id, 1])} />
                </IconButton>
            </CardContent>
        </Card>
    );
}
